import Fibonacci from "./fibonacci.js";

describe('Fibonacci', () => {
  describe('Units', () => {
    test('typed 1 return 0', () => {
      const numberFibonacci = new Fibonacci(1);
      expect(numberFibonacci.toInt()).toEqual(0);
    });
    test('typed 2 return 1', () => {
      const numberFibonacci = new Fibonacci(2);
      expect(numberFibonacci.toInt()).toEqual(1);
    });
    test('typed 3 return 1', () => {
      const numberFibonacci = new Fibonacci(3);
      expect(numberFibonacci.toInt()).toEqual(1);
    });
    test('typed 4 return 2', () => {
      const numberFibonacci = new Fibonacci(4);
      expect(numberFibonacci.toInt()).toEqual(2);
    });
    test('typed 5 return 3', () => {
      const numberFibonacci = new Fibonacci(5);
      expect(numberFibonacci.toInt()).toEqual(3);
    });
    test('typed 6 return 5', () => {
      const numberFibonacci = new Fibonacci(6);
      expect(numberFibonacci.toInt()).toEqual(5);
    });
    test('typed 7 return 8', () => {
      const numberFibonacci = new Fibonacci(7);
      expect(numberFibonacci.toInt()).toEqual(8);
    }); 
    test('typed 8 return 13', () => {
      const numberFibonacci = new Fibonacci(8);
      expect(numberFibonacci.toInt()).toEqual(13);
    }); 
    test('typed 9 return 21', () => {
      const numberFibonacci = new Fibonacci(9);
      expect(numberFibonacci.toInt()).toEqual(21);
    }); 
    test('typed 10 return 34', () => {
      const numberFibonacci = new Fibonacci(10);
      expect(numberFibonacci.toInt()).toEqual(34);
    }); 
    test('typed 11 return 55', () => {
      const numberFibonacci = new Fibonacci(11);
      expect(numberFibonacci.toInt()).toEqual(55);
    }); 

  });

});
