#!/usr/bin/env node

const yargs = require('yargs');
const Fibonacci = require('./fibonacci.js');

module.exports = () => {
  const options = yargs
   .usage("Usage: -n <fibonacci number>")
   .option("n", { alias: "fibonacci-number", describe: "fibonacci number", type: "number", demandOption: true })
   .argv;

  const fn = new Fibonacci(options.n);
    
  if(fn.toInt() == undefined){
    console.log("enter a valid number")
  }else
  console.log(fn.toInt());
}
