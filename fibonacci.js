
class Fibonacci {
  constructor(number){
    this.number = number;
    this.decimal = this.toInt();
 }

  toInt() {
    return(this.decimal);
  }

  toInt() {
    let fibonacci = [];
    fibonacci[0] = 0;
    fibonacci[1] = 1;
    for (var i = 2; i < this.number; i++) {
      fibonacci[i] = fibonacci[i - 2] + fibonacci[i - 1];
    }

    return(fibonacci[this.number-1]);
  }
}

module.exports = Fibonacci;
